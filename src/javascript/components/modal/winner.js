import { createFighterImage } from "../fighterPreview";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 
  let result = {
    title: fighter.name,
    bodyElement:createFighterImage(fighter),
    onClose: function(){ location.reload()}
  }
  showModal(result);
}
