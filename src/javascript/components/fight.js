import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  
  return new Promise((resolve) => {

    let left_fighter_indicator = document.getElementById("left-fighter-indicator");
    let left_fighter_indicator_value = 100;
    let left_fighter_indicator_full_health = firstFighter.health;
    let right_fighter_indicator = document.getElementById("right-fighter-indicator");
    let right_fighter_indicator_value = 100;
    let right_fighter_indicator_full_health = secondFighter.health;
    let criticalAvailability = [true,true];
   

    let downKeys = {}; // the set of keys currently down
    document.addEventListener('keydown', event => {
        downKeys[event.code] = true;
        if (downKeys[controls.PlayerOneAttack] && downKeys[controls.PlayerOneBlock]) {
           console.log("Note: The first player holds the block.");
        }
        else{ 

          if(downKeys[controls.PlayerOneCriticalHitCombination[0]] && downKeys[controls.PlayerOneCriticalHitCombination[1]] && downKeys[controls.PlayerOneCriticalHitCombination[2]] && criticalAvailability[0]){
            let damageDeal = 2*firstFighter.attack;
            console.log("Note: First fighter Critical Damage - "+damageDeal);
            secondFighter.health+=-damageDeal;
            console.log("Note: Second fighter health: "+secondFighter.health)
            right_fighter_indicator_value+= - (100*damageDeal/right_fighter_indicator_full_health);
            right_fighter_indicator.style.width =  right_fighter_indicator_value > 0 ? right_fighter_indicator_value+"%" : 0;
            secondFighter.health<=0 ? resolve(firstFighter) : console.log('');
            criticalAvailability[0] = false;
            setTimeout(() => {
              console.log("Note: First fighter critical hit available.")
              criticalAvailability[0] = true;
            }, 10000);
          }

          if (downKeys[controls.PlayerOneAttack]) {
            let damageDeal = downKeys[controls.PlayerTwoBlock] ? 0: getDamage(firstFighter,secondFighter);
            console.log("Note: Strike from the first fighter - "+damageDeal);
            secondFighter.health+=-damageDeal;
            console.log("Note: Second fighter health: "+secondFighter.health);
            right_fighter_indicator_value+= - (100*damageDeal/right_fighter_indicator_full_health);
            right_fighter_indicator.style.width =  right_fighter_indicator_value > 0 ? right_fighter_indicator_value+"%" : 0;
            console.log("Note: Second fighter health percentage: "+right_fighter_indicator_value);
            secondFighter.health<=0 ? resolve(firstFighter) : console.log('');
         }
        } 
        

        if (downKeys[controls.PlayerTwoAttack] && downKeys[controls.PlayerTwoBlock]) {
          console.log("Note: The second player holds the block.");
        } else{

          if(downKeys[controls.PlayerTwoCriticalHitCombination[0]] && downKeys[controls.PlayerTwoCriticalHitCombination[1]] && downKeys[controls.PlayerTwoCriticalHitCombination[2]] && criticalAvailability[1]){
            let damageDeal = 2*secondFighter.attack;
            console.log("Note: Second fighter Critical Damage - "+damageDeal);
            firstFighter.health+=-damageDeal
            console.log("First fighter health: "+firstFighter.health)
            left_fighter_indicator_value+= - (100*damageDeal/left_fighter_indicator_full_health);
            left_fighter_indicator.style.width =  left_fighter_indicator_value > 0 ? left_fighter_indicator_value+"%" : 0;
            firstFighter.health<=0 ? resolve(secondFighter) : console.log('');
            criticalAvailability[1] = false;
            setTimeout(() => {
              console.log("Note: Second fighter critical hit available.")
              criticalAvailability[1] = true;
            }, 10000);
          }

          if (downKeys[controls.PlayerTwoAttack]) {
        
            let damageDeal = downKeys[controls.PlayerOneBlock] ? 0 : getDamage(secondFighter,firstFighter) ;
            console.log("Note: Strike from the second fighter - "+damageDeal);
            firstFighter.health+=-damageDeal
            console.log("Note: First fighter health: "+firstFighter.health);
            left_fighter_indicator_value+= - (100*damageDeal/left_fighter_indicator_full_health);
            left_fighter_indicator.style.width =  left_fighter_indicator_value > 0 ? left_fighter_indicator_value+"%" : 0;
            console.log("Note: First fighter health percentage: "+left_fighter_indicator_value);
            firstFighter.health<=0 ? resolve(secondFighter) : console.log('');
         }
        }
    });

    document.addEventListener('keyup', event => {
        downKeys[event.code] = false;
    });

  });
}

export function getDamage(attacker, defender) {
  // return damage
  let resultDamage = getHitPower(attacker)-getBlockPower(defender);
  console.log("Note: damage - defense");
  return resultDamage < 0 ? 0 : resultDamage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  return  fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance= Math.random() + 1;
  return fighter.defense * dodgeChance;
}
